#!/usr/bin/gnuplot

reset

set terminal svg size 320,240 font 'Proza Libre,11'
set output 'du-graph-fr.svg'

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

set style line 1 lc rgb 'red' pt 0 ps 1 lt 1 lw 2
set style line 2 lc rgb 'green' pt 0 ps 1 lt 1 lw 2
set style line 3 lc rgb 'yellow' pt 0 ps 1 lt 1 lw 1

set key top right

set xlabel 'Temps (années)'
set ylabel 'Richesse (% de la moyenne)'
set xrange [0:160]
set yrange [0:200]

plot 'du-graph.dat' u 1:3 t 'Alice' w lp ls 1, '' u 1:4 t 'Bob' w lp ls 2, '' u 1:2 t 'Moyenne' w lp ls 3
